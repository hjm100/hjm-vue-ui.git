/**
 * @author hjm100
 * Date: 19/3/27
 */
import Button from "./src/button.vue";
import "./button.css";
Button.install = function(Vue) {
  Vue.component(Button.name, Button);
};

export default Button;
