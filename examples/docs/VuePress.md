# 使用VuePress来管理自己的UI组件

#### 介绍
1. [VuePress](https://vuepress.vuejs.org/zh/) ：Vue 驱动的静态网站生成器

#### 项目架构

```md

.
├─ docs               // 项目文件夹
│  ├─ README.md       // VuePress使用说明
│  └─ .vuepress
│     ├─ dist         // 打包生成文件用在项目部署
│     └─ config.js    // 配置文件
├─ package.json
.

```
#### 使用教程

#### bug汇总

##### 报错： res.getHeader is not a function

1. 解决方法： 安装 npm i webpack-dev-middleware@3.6.0 -D

##### 报错： 使用vue时模板报错

1. 解决方法： script标签一定要与html标签有换行