# Button 按钮
----
### 基础用法

使用```type```、```size```、 ```disabled```和```round```属性来定义 Button 的样式。

### 效果展示

<div class="demo-block">
  <hjm-button>默认按钮</hjm-button>
  <hjm-button type="primary">主要按钮</hjm-button>
  <hjm-button type="success">成功按钮</hjm-button>
  <hjm-button type="warning">错误按钮</hjm-button>
  <hjm-button type="danger" :disabled="true">危险按钮</hjm-button>
  <hjm-button type="info" size="mini">信息按钮</hjm-button>
  <hjm-button type="primary" :round="true" size="mini">圆角按钮</hjm-button>
</div>

<script>
import Vue from "vue";
import hjmui from "../../packages/index.js";
Vue.use(hjmui.hjmButton);
export default {
  data() {
    return {};
  },
  mounted() {
    // 这里模拟数据请求
    console.log('你好')
  },
};
</script>

<style>
  .demo-block{
    display: block;
    padding:10px 20px;
  }
  .demo-block button{
    margin:10px;
    color:#fff;
  }
</style>

### 代码演示

```html

  <hjm-button>默认按钮</hjm-button>
  <hjm-button type="primary">主要按钮</hjm-button>
  <hjm-button type="success">成功按钮</hjm-button>
  <hjm-button type="warning">错误按钮</hjm-button>
  <hjm-button type="danger" :disabled="true">危险按钮</hjm-button>
  <hjm-button type="info" size="mini">信息按钮</hjm-button>
  <hjm-button type="primary" :round="true" size="mini">圆角按钮</hjm-button>

```

### hjmButton api

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| :-: | :-: | :-: | :-: | :-: |
| type | 类型，可选值为 primary info warning danger success| String | primary | - |
| size | 尺寸，可选值为 large small mini| String | large | - |
| disabled | 是否禁用按钮 | Boolean | false | - |
| round | 是否为圆形按钮 | Boolean | false | - |

### hjmButton Event

| 事件名 | 说明 | 参数 |
| :-: | :-: | :-: | 
| click | 点击按钮，且按钮状态不为加载或禁用时触发 | - |

### hjmButton Slot

| 名称 | 说明 |
| :-: | :-: |
| - | 自定义button显示内容 |

