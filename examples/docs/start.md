
# 快速上手

----

#### 使用教程

1. 安装npm包： npm i hjm-vue-ui

1. 在项目中使用

```js

//在main.js注入
// 方法一: 直接引入
// 导入组件库
import hjmui from "hjm-vue-ui";
// 注册组件库
Vue.use(hjmui.hjmButton); //单独注入

// 方法二: 使用打包后文件
import "hjm-vue-ui/lib/hjmui.css";
import hjmui from "hjm-vue-ui/lib/hjmui.umd.min";
Vue.use(hjmui.hjmButton);

// 在项目中使用过滤器

<hjm-button class="btn" :disabled="true" type="success" size="mini">
  <p>你好小白</p>
</hjm-button>

```

## 标准开发

1. examples目录下为例子说明(后期添加markdown)
1. packages下面为ui组件开发(一定要规范)
1. 单个UI导出，分类别区分UI组件


1. 效果测试与查看
```js
// main.js
// 导入组件库
import { hjmui } from "../packages/index";
// 注册组件库
Vue.use(hjmui);

```

1. index.html中测试

```html
  <hjm-button class="btn" :disabled="true" type="success" size="mini">
    <p>你好小白</p>
  </hjm-button>

```
#### 运行项目

1. npm run serve 运行项目demo查看测试效果
2. npm run build 打包项目（可以用于生成官网）
3. npm run lib 打包ui组件库文件，支持js引入(注意：发布包前需要打包组件)