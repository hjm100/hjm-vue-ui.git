# 开发教程

#### 介绍
1. 开发属于自己的ui组件库

#### 项目架构

```md
.
|—— lib                       // 打包生成文件
|—— examples                  // 例子文件夹(对应vue-cli创建项目的src文件夹)
|   |—— assets                // 例子所用资源文件
|   |—— components            // 例子组件库
|   |—— App.vue               // vue首页
|   └── main.js              
|—— packages                  // UI组件源码
|   |—— button                // 组件文件
|   |   |—— src               
|   |   |   └── button.js     // 组件
|   |   |—— button.css        // 组件样式
|   |   └── index.js          // 组件入口
|   |—— style                 // 组件公用css样式
|   |   |—— common            
|   |   |   └── var.css       // css变量
|   |   |—— fonts             // 字体图标库
|   |   └── reset.css         // 样式重置
|   └── index.js              // 组件方法汇总    
|—— public                    // 静态页面
|—— .eslintrc.js
|—— .gitignore                // 限制git上传文件
|—— .browserslistrc
|—— babel.config.js
|—— package.json              // 本地npm管理文件  
|—— JEST.md                   // 单元测试说明(暂时没有集成)
|—— CHANGELOG.md              // 更新日志
|—— README.md                 // 项目说明
|—— vue.config.js             // vue脚手架配置
.
```
#### 使用教程

1. 安装npm包： npm i hjm-vue-ui

1. 在项目中使用

```js

//在main.js注入

// 方法一: 直接引入

// 导入组件库
import hjmui from "hjm-vue-ui";
// 注册组件库
Vue.use(hjmui.hjmButton); //单独注入

// 方法二: 使用打包后文件
import "hjm-vue-ui/lib/hjmui.css";
import hjmui from "hjm-vue-ui/lib/hjmui.umd.min";
Vue.use(hjmui.hjmButton);

// 在项目中使用过滤器

<hjm-button class="btn" :disabled="true" type="success" size="mini">
  <p>你好小白</p>
</hjm-button>

```

#### 开发说明

1. examples目录下为例子说明(后期添加markdown)
1. packages下面为ui组件开发(一定要规范)
1. 单个UI导出，分类别区分UI组件


1. 效果测试与查看
```js
// main.js
// 导入组件库
import { hjmui } from "../packages/index";
// 注册组件库
Vue.use(hjmui);

```

1. index.html中测试

```html
  <hjm-button class="btn" :disabled="true" type="success" size="mini">
    <p>你好小白</p>
  </hjm-button>

```

#### 运行项目

1. npm run serve 运行项目demo查看测试效果
2. npm run build 打包项目（可以用于生成官网）
3. npm run lib 打包ui组件库文件，支持js引入(注意：发布包前需要打包组件)